from django.apps import AppConfig


class GiftwishappConfig(AppConfig):
    name = 'GiftWishApp'
