# Generated by Django 2.2.7 on 2019-11-19 20:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('GiftWishApp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='gift',
            name='user_id',
            field=models.IntegerField(default=0),
        ),
    ]
