from django.db import models


# Create your models here.
class User(models.Model):
    email_address = models.CharField(max_length=30)
    username = models.CharField(max_length=30)
    password = models.CharField(max_length=30)

    @property
    def gifts(self): #gets all gifts associated with this user
        gifts = Gift.objects.all()
        if gifts.exists():  # if there are any gifts, we only want the ones associated with this user_id
            gifts = gifts.filter(user_id=self.id)
        return gifts


class Gift(models.Model):
    name = models.CharField(max_length=30)
    user_id = models.IntegerField(default=0)
