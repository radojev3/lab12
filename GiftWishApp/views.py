from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render
from django.views import View

# Create your views here.
from GiftWishApp.models import Gift, User


class home(View):
    def get(self, request):
        return render(request, "home.html")

    def post(self, request):
        try:
            user = User.objects.get(username=request.POST['username'])
            if user.password == request.POST[
                'password']:  # assigns a user_id to the session if user successfully logged in
                request.session['user_id'] = user.id
                return render(request, "home.html")
        except ObjectDoesNotExist:
            return render(request, "home.html")


class registration(View):
    def get(self, request):
        return render(request, "registration.html")

    def post(self, request):
        # gathers info needed for user model
        email_address: str = request.POST['email_address']
        username: str = request.POST['username']
        password: str = request.POST['password']

        User.objects.create(email_address=email_address, username=username, password=password).save()
        return render(request, "success.html")


class users(View):
    def get(self, request):
        users = User.objects.all()
        return render(request, "users.html", {'users': users})


class gifts(View):
    def get(self, request):
        if 'user_id' in request.session:
            user = User.objects.get(id=request.session['user_id'])
            return render(request, "gifts.html", {'Gifts': user.gifts})
        else:
            return render(request, "gifts.html", {'Gifts': {}})

    def post(self, request):
        user = User.objects.get(id=request.session['user_id'])
        name = request.POST['name']
        user_id = request.session['user_id']

        Gift.objects.create(name=name, user_id=user_id).save()
        return render(request, "gifts.html", {'Gifts': user.gifts})
